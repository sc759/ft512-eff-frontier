CFLAGS= -Wall -Werror -std=gnu++98 -pedantic -ggdb3

efficient_frontier: ef.cpp
	g++ $(CFLAGS) -o efficient_frontier ef.cpp

clean:
	rm -f efficient_frontier

