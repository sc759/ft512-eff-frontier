#include <iostream>
#include <exception>
#include <fstream>
#include <cstdlib>
#include <vector>
#include <string>
#include <string.h>
#include <Eigen/Dense>
#include <math.h>
#include <iomanip>

using namespace Eigen;

class invalid_input : public std::exception {
        private:
                std::string message;
        public:
                invalid_input(const char* msg) : message(msg) {

                }

                invalid_input(std::string & msg) : message(msg) {

                }

                virtual ~invalid_input() throw () {

                }

                virtual const char *what() const throw () {
                        return message.c_str();
                }
};

void parse_line(std::string const & line, MatrixXd & target, int lpos, int sz) {
	std::size_t l = 0;
	std::size_t r = line.find(',', l);
	int pos = 0;
	while (r != std::string::npos) {
		target(lpos, pos) = atof(line.substr(l, r - l).data());
		l = r + 1;
		r = line.find(',', l);
		pos++;
		if (pos >= sz) {
			std::cerr << "extra element: pos = " << pos << ", sz = " << sz << std::endl;
			throw invalid_input("extra element");
		}
	}
	target(lpos, pos) = atof(line.substr(l).data());
	if (pos != sz - 1) {
		std::cerr << "less element: pos = " << pos << ", sz = " << sz << std::endl;
		throw invalid_input("less element");
	}
}

void convert(std::vector<double> const & src, VectorXd & tgt) {
	int size = (int) (src.size());
	if (size != tgt.rows()) {
		std::cerr << "fatal error\n";
		exit(EXIT_FAILURE);
	}
	for (int i = 0; i < size; i++) {
		tgt(i) = src[i];
	}
}

class Data {
	private:
		std::vector<double> ror;
		std::vector<double> sdv;
		int size;
		VectorXd rors;
	       	VectorXd sdvs;
		MatrixXd cors;

	public:
		Data(std::ifstream & uni_f, std::ifstream & corr_f) {
			build_uni(uni_f);
			build_corr(corr_f);
		}

		void build_uni(std::ifstream & ifs) {
			std::string line;
			size = 0;
			if (ifs.fail()) {
                                throw invalid_input("cannot read the first line");
                        }
                        std::getline(ifs, line);
			while (ifs.good()) {
				std::size_t start = line.find(',', 0) + 1;
				if (start == std::string::npos) {
					std::cerr << "error in reading " << line << std::endl;
					throw invalid_input("error in reading");
				}
				std::size_t pos = line.find(',', start);
				if (pos == std::string::npos) {
                                        std::cerr << "error in reading " << line << std::endl;
                                        throw invalid_input("error in reading");
                                }
				ror.push_back(atof(line.substr(start, pos).data()));
				sdv.push_back(atof(line.substr(pos + 1).data()));
				size++;
				std::getline(ifs, line);
			}
			rors.resize(size);
			sdvs.resize(size);
			convert(ror, rors);
			convert(sdv, sdvs);
		}

		void build_corr(std::ifstream & ifs) {
			std::string line;
			if (ifs.fail()) {
                                throw invalid_input("cannot read the first line");
                        }
			std::getline(ifs, line);
			int count = 0;
			cors.resize(size, size);
			while (ifs.good()) {
				if (count == size) {
					throw invalid_input("extra matrix line");
				}
				parse_line(line, cors, count, size);
				count++;
				std::getline(ifs, line);
			}
			if (count != size) {
				throw invalid_input("incorrect matrix size");
			}
			cors = sdvs.asDiagonal() * cors * sdvs.asDiagonal();
		}

		void calc_unrestricted() {
			std::cout << "ROR,volatility" << std::endl;
			MatrixXd A = MatrixXd(size + 2, size + 2);
			A << cors, rors, MatrixXd::Ones(size, 1),
			     rors.transpose(), MatrixXd::Zero(1, 2),
			     MatrixXd::Ones(1, size), MatrixXd::Zero(1, 2);
			VectorXd b = VectorXd(size + 2);
			for (int i = 1; i <= 26; i++) {
				double r_p = i / (double) 100;
				b << VectorXd::Zero(size), r_p, 1;
				VectorXd x = A.fullPivLu().solve(b).head(size);
				double stdv_p = sqrt(x.transpose() * cors * x) * 100;
				std::cout << std::fixed << std::setprecision(1) << r_p * 100 << "%,";
				std::cout << std::fixed << std::setprecision(2) << stdv_p << "%" << std::endl;
			}
		}

		void calc_restricted() {
			std::cout << "ROR,volatility" << std::endl;
			MatrixXd A = MatrixXd(size + 2, size + 2);
			VectorXd b = VectorXd(size + 2);
			for (int i = 1; i <= 26; i++) {
				double r_p = i / (double) 100;
				bool valid = false;
				A << cors, rors, MatrixXd::Ones(size, 1),
                     		     rors.transpose(), MatrixXd::Zero(1, 2),
                                     MatrixXd::Ones(1, size), MatrixXd::Zero(1, 2);
				b << VectorXd::Zero(size), r_p, 1;
				VectorXd x;
				while (!valid) {
					x = A.fullPivLu().solve(b).head(size);
					valid = true;
					for (int j = 0; j < size; j++) {
						if (x(j) < 0) {
							for (int k = 0; k < size + 2; k++) {
								if (j == k) {
									A(j, k) = 1;
								} else {
									A(j, k) = 0;
								}
							}
							valid = false;
						}
					}
				}
				double stdv_p = sqrt(x.transpose() * cors * x) * 100;
                                std::cout << std::fixed << std::setprecision(1) << r_p * 100 << "%,";
                                std::cout << std::fixed << std::setprecision(2) << stdv_p << "%" << std::endl;
			}
		}

		void print() {
			std::cout << "---rors---" << std::endl;
			std::cout << rors << std::endl;
			std::cout << "---sdvs---" << std::endl;
                        std::cout << sdvs << std::endl;
			std::cout << "---cors---" << std::endl;
                        std::cout << cors << std::endl;
		}
};

int main(int argc, char ** argv) {
	std::ifstream uni_f;
	std::ifstream corr_f;
	bool restricted = false;
	if (argc < 3 || argc > 4) {
		std::cerr << "usage: efficient_frontier [-r] <uni_file> <corr_file>\n";
		return EXIT_FAILURE;
	}
	int uni_pos = -1;
	int corr_pos = -1;
	for (int i = 1; i < argc; i++) {
		if (strcmp(argv[i], "-r") == 0) {
			restricted = true;
			continue;
		}
		if (uni_pos == -1) {
			uni_pos = i;
		} else {
			corr_pos = i;
		}
	}
	uni_f.open(argv[uni_pos]);
	if (!uni_f.is_open()) {
		std::cerr << "cannot open " << argv[1] << std::endl;
		return EXIT_FAILURE;
	}
	corr_f.open(argv[corr_pos]);
	if (!corr_f.is_open()) {
		std::cerr << "cannot open " << argv[2] << std::endl;
		return EXIT_FAILURE;
	}
	try {
		Data d(uni_f, corr_f);
		if (restricted) {
			d.calc_restricted();
		} else {
			d.calc_unrestricted();
		}
	} catch (invalid_input & e) {
                std::cerr << e.what() << std::endl;
                return EXIT_FAILURE;
        }
	uni_f.close();
	corr_f.close();
	return EXIT_SUCCESS;
}
