#include <iostream>
#include <string>
#include <vector>
#include <Eigen/Dense>

using namespace Eigen;

int main() {
	MatrixXd a = MatrixXd(2, 3);
	a << 1, 2, 3,
	     4, 5, 6;
	std::cout << a << std::endl;
	return 0;
}
